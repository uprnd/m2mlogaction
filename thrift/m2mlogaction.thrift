namespace java thriftif.up.core.m2m.logaction
namespace cpp Up.Core.M2M.LogAction

enum ErrorCode {
	ERR_NONE = 0,
	ERR_UNKNOWN,
	ERR_ITEM_NOT_FOUND,
	ERR_ITEM_EXIST,
	ERR_INVALID_PARAM,
	ERR_CONNECTION
}

typedef i64 TKey

enum ActionCode {
    OPEN = 1
}

struct TValue
{
    1: i16 action;
    2: i64 time;
    3: i32 userId;
    4: string userName,
    5: string doorKey,
    6: i32 doorId,
    7: string doorName,
    8: i16 errCode,
    9: optional string errDescription

}

struct TValueResult {
	1: i16 error = -1;
	2: optional TValue value;
}

typedef list<TKey> TKeyList
struct TValueResultMap
{
    1: TKey tKey;
    2: TValueResult tValueResult;
}
typedef list<TValueResultMap> TValueResultMapList

service TM2MLogActionService {

	//common data item functions
	TValueResult getK(1: TKey key);
	TValueResult putK(1: TKey key, 2: TValue value);
	TValueResult removeK(1: TKey key);		
	TValueResultMapList listK(1: i64 start, 2: i32 limit);	
}

