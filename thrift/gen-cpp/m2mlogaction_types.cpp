/**
 * Autogenerated by Thrift Compiler (0.9.1)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
#include "m2mlogaction_types.h"

#include <algorithm>

namespace Up { namespace Core { namespace M2M { namespace LogAction {

int _kErrorCodeValues[] = {
  ErrorCode::ERR_NONE,
  ErrorCode::ERR_UNKNOWN,
  ErrorCode::ERR_ITEM_NOT_FOUND,
  ErrorCode::ERR_ITEM_EXIST,
  ErrorCode::ERR_INVALID_PARAM,
  ErrorCode::ERR_CONNECTION
};
const char* _kErrorCodeNames[] = {
  "ERR_NONE",
  "ERR_UNKNOWN",
  "ERR_ITEM_NOT_FOUND",
  "ERR_ITEM_EXIST",
  "ERR_INVALID_PARAM",
  "ERR_CONNECTION"
};
const std::map<int, const char*> _ErrorCode_VALUES_TO_NAMES(::apache::thrift::TEnumIterator(6, _kErrorCodeValues, _kErrorCodeNames), ::apache::thrift::TEnumIterator(-1, NULL, NULL));

int _kActionCodeValues[] = {
  ActionCode::OPEN
};
const char* _kActionCodeNames[] = {
  "OPEN"
};
const std::map<int, const char*> _ActionCode_VALUES_TO_NAMES(::apache::thrift::TEnumIterator(1, _kActionCodeValues, _kActionCodeNames), ::apache::thrift::TEnumIterator(-1, NULL, NULL));

const char* TValue::ascii_fingerprint = "112E8C355A4F0B9E02CD85A19ECF4CA2";
const uint8_t TValue::binary_fingerprint[16] = {0x11,0x2E,0x8C,0x35,0x5A,0x4F,0x0B,0x9E,0x02,0xCD,0x85,0xA1,0x9E,0xCF,0x4C,0xA2};

uint32_t TValue::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_I16) {
          xfer += iprot->readI16(this->action);
          this->__isset.action = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_I64) {
          xfer += iprot->readI64(this->time);
          this->__isset.time = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 3:
        if (ftype == ::apache::thrift::protocol::T_I32) {
          xfer += iprot->readI32(this->userId);
          this->__isset.userId = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 4:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->userName);
          this->__isset.userName = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 5:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->doorKey);
          this->__isset.doorKey = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 6:
        if (ftype == ::apache::thrift::protocol::T_I32) {
          xfer += iprot->readI32(this->doorId);
          this->__isset.doorId = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 7:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->doorName);
          this->__isset.doorName = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 8:
        if (ftype == ::apache::thrift::protocol::T_I16) {
          xfer += iprot->readI16(this->errCode);
          this->__isset.errCode = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 9:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->errDescription);
          this->__isset.errDescription = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t TValue::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("TValue");

  xfer += oprot->writeFieldBegin("action", ::apache::thrift::protocol::T_I16, 1);
  xfer += oprot->writeI16(this->action);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("time", ::apache::thrift::protocol::T_I64, 2);
  xfer += oprot->writeI64(this->time);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("userId", ::apache::thrift::protocol::T_I32, 3);
  xfer += oprot->writeI32(this->userId);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("userName", ::apache::thrift::protocol::T_STRING, 4);
  xfer += oprot->writeString(this->userName);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("doorKey", ::apache::thrift::protocol::T_STRING, 5);
  xfer += oprot->writeString(this->doorKey);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("doorId", ::apache::thrift::protocol::T_I32, 6);
  xfer += oprot->writeI32(this->doorId);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("doorName", ::apache::thrift::protocol::T_STRING, 7);
  xfer += oprot->writeString(this->doorName);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("errCode", ::apache::thrift::protocol::T_I16, 8);
  xfer += oprot->writeI16(this->errCode);
  xfer += oprot->writeFieldEnd();

  if (this->__isset.errDescription) {
    xfer += oprot->writeFieldBegin("errDescription", ::apache::thrift::protocol::T_STRING, 9);
    xfer += oprot->writeString(this->errDescription);
    xfer += oprot->writeFieldEnd();
  }
  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(TValue &a, TValue &b) {
  using ::std::swap;
  swap(a.action, b.action);
  swap(a.time, b.time);
  swap(a.userId, b.userId);
  swap(a.userName, b.userName);
  swap(a.doorKey, b.doorKey);
  swap(a.doorId, b.doorId);
  swap(a.doorName, b.doorName);
  swap(a.errCode, b.errCode);
  swap(a.errDescription, b.errDescription);
  swap(a.__isset, b.__isset);
}

const char* TValueResult::ascii_fingerprint = "236DB74F737074404B1E2D5EA386F26E";
const uint8_t TValueResult::binary_fingerprint[16] = {0x23,0x6D,0xB7,0x4F,0x73,0x70,0x74,0x40,0x4B,0x1E,0x2D,0x5E,0xA3,0x86,0xF2,0x6E};

uint32_t TValueResult::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_I16) {
          xfer += iprot->readI16(this->error);
          this->__isset.error = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_STRUCT) {
          xfer += this->value.read(iprot);
          this->__isset.value = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t TValueResult::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("TValueResult");

  xfer += oprot->writeFieldBegin("error", ::apache::thrift::protocol::T_I16, 1);
  xfer += oprot->writeI16(this->error);
  xfer += oprot->writeFieldEnd();

  if (this->__isset.value) {
    xfer += oprot->writeFieldBegin("value", ::apache::thrift::protocol::T_STRUCT, 2);
    xfer += this->value.write(oprot);
    xfer += oprot->writeFieldEnd();
  }
  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(TValueResult &a, TValueResult &b) {
  using ::std::swap;
  swap(a.error, b.error);
  swap(a.value, b.value);
  swap(a.__isset, b.__isset);
}

const char* TValueResultMap::ascii_fingerprint = "9C3105FB76FFFD1B6DA2946CB481A43C";
const uint8_t TValueResultMap::binary_fingerprint[16] = {0x9C,0x31,0x05,0xFB,0x76,0xFF,0xFD,0x1B,0x6D,0xA2,0x94,0x6C,0xB4,0x81,0xA4,0x3C};

uint32_t TValueResultMap::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_I64) {
          xfer += iprot->readI64(this->tKey);
          this->__isset.tKey = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_STRUCT) {
          xfer += this->tValueResult.read(iprot);
          this->__isset.tValueResult = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t TValueResultMap::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("TValueResultMap");

  xfer += oprot->writeFieldBegin("tKey", ::apache::thrift::protocol::T_I64, 1);
  xfer += oprot->writeI64(this->tKey);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("tValueResult", ::apache::thrift::protocol::T_STRUCT, 2);
  xfer += this->tValueResult.write(oprot);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(TValueResultMap &a, TValueResultMap &b) {
  using ::std::swap;
  swap(a.tKey, b.tKey);
  swap(a.tValueResult, b.tValueResult);
  swap(a.__isset, b.__isset);
}

}}}} // namespace
