#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/main.o \
	${OBJECTDIR}/src/ModelData.o \
	${OBJECTDIR}/src/ServerApp.o \
	${OBJECTDIR}/src/ServiceFactory.o \
	${OBJECTDIR}/src/ZServiceModel.o \
	${OBJECTDIR}/thrift/gen-cpp/TM2MLogActionService.o \
	${OBJECTDIR}/thrift/gen-cpp/m2mlogaction_constants.o \
	${OBJECTDIR}/thrift/gen-cpp/m2mlogaction_types.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-std=c++0x
CXXFLAGS=-std=c++0x

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=../corelibs/UPStoreAppCommon/lib/libupstoreappcommon.a ../corelibs/UPStorage/lib/libupstorage.a ../corelibs/UPDistributed/lib/libupdistributed.a ../corelibs/UPCaching/lib/libupcaching.a ../corelibs/UPHashing/lib/libuphashing.a ../corelibs/UPBase/lib/libupbase.a ../corelibs/UPThrift/lib/libupthrift.a ../corelibs/UPPoco/lib/libuppoco.a ../corelibs/UPEvent/lib/libupevent.a ../corelibs/UPMalloc/lib/libupmalloc.a -lpthread -ldl -lrt

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk bin/m2mlogactionservice

bin/m2mlogactionservice: ../corelibs/UPStoreAppCommon/lib/libupstoreappcommon.a

bin/m2mlogactionservice: ../corelibs/UPStorage/lib/libupstorage.a

bin/m2mlogactionservice: ../corelibs/UPDistributed/lib/libupdistributed.a

bin/m2mlogactionservice: ../corelibs/UPCaching/lib/libupcaching.a

bin/m2mlogactionservice: ../corelibs/UPHashing/lib/libuphashing.a

bin/m2mlogactionservice: ../corelibs/UPBase/lib/libupbase.a

bin/m2mlogactionservice: ../corelibs/UPThrift/lib/libupthrift.a

bin/m2mlogactionservice: ../corelibs/UPPoco/lib/libuppoco.a

bin/m2mlogactionservice: ../corelibs/UPEvent/lib/libupevent.a

bin/m2mlogactionservice: ../corelibs/UPMalloc/lib/libupmalloc.a

bin/m2mlogactionservice: ${OBJECTFILES}
	${MKDIR} -p bin
	${LINK.cc} -o bin/m2mlogactionservice ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/main.o: main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DHAVE_CONFIG_H -Iinc -Ithrift/gen-cpp -I../corelibs/UPBoost/inc -I../corelibs/UPThrift/inc -I../corelibs/UPThrift/thrift_src -I../corelibs/UPThrift/thrift_src/thrift -I../corelibs/UPPoco/inc -I../corelibs/UPCaching/inc/sparsehash -I../corelibs/UPCaching/inc -I../corelibs/UPCaching -I../corelibs/UPBase -I../corelibs/UPBase/inc -I../corelibs/UPBase/upframework -I../corelibs/UPBase/upframework/upbase/if/gen-cpp -I../corelibs/UPBase/upframework/upbase -I../corelibs/UPHashing/inc -I../corelibs/UPStorage/inc -I../corelibs/UPStoreAppCommon -I../corelibs/UPStoreAppCommon/inc -I../corelibs/UPStoreAppCommon/if/thrift/gen-cpp -I../corelibs/UPEvent/inc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

${OBJECTDIR}/src/ModelData.o: src/ModelData.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DHAVE_CONFIG_H -Iinc -Ithrift/gen-cpp -I../corelibs/UPBoost/inc -I../corelibs/UPThrift/inc -I../corelibs/UPThrift/thrift_src -I../corelibs/UPThrift/thrift_src/thrift -I../corelibs/UPPoco/inc -I../corelibs/UPCaching/inc/sparsehash -I../corelibs/UPCaching/inc -I../corelibs/UPCaching -I../corelibs/UPBase -I../corelibs/UPBase/inc -I../corelibs/UPBase/upframework -I../corelibs/UPBase/upframework/upbase/if/gen-cpp -I../corelibs/UPBase/upframework/upbase -I../corelibs/UPHashing/inc -I../corelibs/UPStorage/inc -I../corelibs/UPStoreAppCommon -I../corelibs/UPStoreAppCommon/inc -I../corelibs/UPStoreAppCommon/if/thrift/gen-cpp -I../corelibs/UPEvent/inc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/ModelData.o src/ModelData.cpp

${OBJECTDIR}/src/ServerApp.o: src/ServerApp.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DHAVE_CONFIG_H -Iinc -Ithrift/gen-cpp -I../corelibs/UPBoost/inc -I../corelibs/UPThrift/inc -I../corelibs/UPThrift/thrift_src -I../corelibs/UPThrift/thrift_src/thrift -I../corelibs/UPPoco/inc -I../corelibs/UPCaching/inc/sparsehash -I../corelibs/UPCaching/inc -I../corelibs/UPCaching -I../corelibs/UPBase -I../corelibs/UPBase/inc -I../corelibs/UPBase/upframework -I../corelibs/UPBase/upframework/upbase/if/gen-cpp -I../corelibs/UPBase/upframework/upbase -I../corelibs/UPHashing/inc -I../corelibs/UPStorage/inc -I../corelibs/UPStoreAppCommon -I../corelibs/UPStoreAppCommon/inc -I../corelibs/UPStoreAppCommon/if/thrift/gen-cpp -I../corelibs/UPEvent/inc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/ServerApp.o src/ServerApp.cpp

${OBJECTDIR}/src/ServiceFactory.o: src/ServiceFactory.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DHAVE_CONFIG_H -Iinc -Ithrift/gen-cpp -I../corelibs/UPBoost/inc -I../corelibs/UPThrift/inc -I../corelibs/UPThrift/thrift_src -I../corelibs/UPThrift/thrift_src/thrift -I../corelibs/UPPoco/inc -I../corelibs/UPCaching/inc/sparsehash -I../corelibs/UPCaching/inc -I../corelibs/UPCaching -I../corelibs/UPBase -I../corelibs/UPBase/inc -I../corelibs/UPBase/upframework -I../corelibs/UPBase/upframework/upbase/if/gen-cpp -I../corelibs/UPBase/upframework/upbase -I../corelibs/UPHashing/inc -I../corelibs/UPStorage/inc -I../corelibs/UPStoreAppCommon -I../corelibs/UPStoreAppCommon/inc -I../corelibs/UPStoreAppCommon/if/thrift/gen-cpp -I../corelibs/UPEvent/inc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/ServiceFactory.o src/ServiceFactory.cpp

${OBJECTDIR}/src/ZServiceModel.o: src/ZServiceModel.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DHAVE_CONFIG_H -Iinc -Ithrift/gen-cpp -I../corelibs/UPBoost/inc -I../corelibs/UPThrift/inc -I../corelibs/UPThrift/thrift_src -I../corelibs/UPThrift/thrift_src/thrift -I../corelibs/UPPoco/inc -I../corelibs/UPCaching/inc/sparsehash -I../corelibs/UPCaching/inc -I../corelibs/UPCaching -I../corelibs/UPBase -I../corelibs/UPBase/inc -I../corelibs/UPBase/upframework -I../corelibs/UPBase/upframework/upbase/if/gen-cpp -I../corelibs/UPBase/upframework/upbase -I../corelibs/UPHashing/inc -I../corelibs/UPStorage/inc -I../corelibs/UPStoreAppCommon -I../corelibs/UPStoreAppCommon/inc -I../corelibs/UPStoreAppCommon/if/thrift/gen-cpp -I../corelibs/UPEvent/inc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/ZServiceModel.o src/ZServiceModel.cpp

${OBJECTDIR}/thrift/gen-cpp/TM2MLogActionService.o: thrift/gen-cpp/TM2MLogActionService.cpp 
	${MKDIR} -p ${OBJECTDIR}/thrift/gen-cpp
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DHAVE_CONFIG_H -Iinc -Ithrift/gen-cpp -I../corelibs/UPBoost/inc -I../corelibs/UPThrift/inc -I../corelibs/UPThrift/thrift_src -I../corelibs/UPThrift/thrift_src/thrift -I../corelibs/UPPoco/inc -I../corelibs/UPCaching/inc/sparsehash -I../corelibs/UPCaching/inc -I../corelibs/UPCaching -I../corelibs/UPBase -I../corelibs/UPBase/inc -I../corelibs/UPBase/upframework -I../corelibs/UPBase/upframework/upbase/if/gen-cpp -I../corelibs/UPBase/upframework/upbase -I../corelibs/UPHashing/inc -I../corelibs/UPStorage/inc -I../corelibs/UPStoreAppCommon -I../corelibs/UPStoreAppCommon/inc -I../corelibs/UPStoreAppCommon/if/thrift/gen-cpp -I../corelibs/UPEvent/inc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/thrift/gen-cpp/TM2MLogActionService.o thrift/gen-cpp/TM2MLogActionService.cpp

${OBJECTDIR}/thrift/gen-cpp/m2mlogaction_constants.o: thrift/gen-cpp/m2mlogaction_constants.cpp 
	${MKDIR} -p ${OBJECTDIR}/thrift/gen-cpp
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DHAVE_CONFIG_H -Iinc -Ithrift/gen-cpp -I../corelibs/UPBoost/inc -I../corelibs/UPThrift/inc -I../corelibs/UPThrift/thrift_src -I../corelibs/UPThrift/thrift_src/thrift -I../corelibs/UPPoco/inc -I../corelibs/UPCaching/inc/sparsehash -I../corelibs/UPCaching/inc -I../corelibs/UPCaching -I../corelibs/UPBase -I../corelibs/UPBase/inc -I../corelibs/UPBase/upframework -I../corelibs/UPBase/upframework/upbase/if/gen-cpp -I../corelibs/UPBase/upframework/upbase -I../corelibs/UPHashing/inc -I../corelibs/UPStorage/inc -I../corelibs/UPStoreAppCommon -I../corelibs/UPStoreAppCommon/inc -I../corelibs/UPStoreAppCommon/if/thrift/gen-cpp -I../corelibs/UPEvent/inc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/thrift/gen-cpp/m2mlogaction_constants.o thrift/gen-cpp/m2mlogaction_constants.cpp

${OBJECTDIR}/thrift/gen-cpp/m2mlogaction_types.o: thrift/gen-cpp/m2mlogaction_types.cpp 
	${MKDIR} -p ${OBJECTDIR}/thrift/gen-cpp
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DHAVE_CONFIG_H -Iinc -Ithrift/gen-cpp -I../corelibs/UPBoost/inc -I../corelibs/UPThrift/inc -I../corelibs/UPThrift/thrift_src -I../corelibs/UPThrift/thrift_src/thrift -I../corelibs/UPPoco/inc -I../corelibs/UPCaching/inc/sparsehash -I../corelibs/UPCaching/inc -I../corelibs/UPCaching -I../corelibs/UPBase -I../corelibs/UPBase/inc -I../corelibs/UPBase/upframework -I../corelibs/UPBase/upframework/upbase/if/gen-cpp -I../corelibs/UPBase/upframework/upbase -I../corelibs/UPHashing/inc -I../corelibs/UPStorage/inc -I../corelibs/UPStoreAppCommon -I../corelibs/UPStoreAppCommon/inc -I../corelibs/UPStoreAppCommon/if/thrift/gen-cpp -I../corelibs/UPEvent/inc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/thrift/gen-cpp/m2mlogaction_types.o thrift/gen-cpp/m2mlogaction_types.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} bin/m2mlogactionservice

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
