/* 
 * File:   ServerApp.cpp
 * Author: trungthanh
 * 
 * Created on February 23, 2012, 6:18 PM
 */

#include "ServerApp.h"

#include <ZServiceModel.h>
#include <ServiceFactory.h>
#include <ServiceHandler.h>

#include <monitor/TStorageStatModule.h>
#include <monitor/TStorageMonitorThriftHandler.h>

using namespace Up::Core::M2M::LogAction;

ServerApp::ServerApp() : _showHelp(false) {
}

ServerApp::~ServerApp() {
}

void ServerApp::initialize(Poco::Util::Application& app) {
    if (_showHelp)
	return;

    loadConfiguration();
    if (config().has("app.showhelp")) {
	return;
    }
    ServiceFactory::init(app);

    ServiceHandler::ThriftServer* aBizServer = new ServiceHandler::ThriftServer(
	    ServiceFactory::_svrPort
	    , ServiceFactory::_workerCount
	    , boost::shared_ptr< ServiceHandler > (new ServiceHandler(ServiceFactory::getModel())));
    this->addSubsystem(aBizServer);
    TStorageStatModule* statModule = new TStorageStatModule();
    this->addSubsystem(statModule);

    //Stat handler
    TStorageMonitorThriftHandler* aStatHandler = new TStorageMonitorThriftHandler;

    this->addSubsystem(new TStorageMonitorThriftHandler::TStorageMonitorServer(
	    ServiceFactory::_cfgPort
	    , 2
	    , boost::shared_ptr< up::up101::storage::monitor::StorageMonitorServiceIf > (aStatHandler)
	    ));

    //Initialize subsystems
    Poco::Util::ServerApplication::initialize(app);

    aStatHandler->setCacheStorage(ServiceFactory::getCache().get(), ServiceFactory::getStorage().get(), ServiceFactory::getKVStorage().get());
    TStorageStatModule::getInstance().setStatusFetcher(ServiceFactory::getStatFetcher());
    ServiceFactory::getStorage()->setObserver(&TStorageStatModule::getInstance());
    aStatHandler->setServer(aBizServer->thriftServer());
}

int ServerApp::main(const std::vector<std::string>& args) {
    if (config().has("app.showhelp")) {
	showHelp();
	return 0;
    }
    Poco::Util::ServerApplication::main(args);
    try {
	if (!ServiceFactory::_zkServers.empty()) {
	    this->_zkReg.setZkHosts(ServiceFactory::_zkServers);
	    if (this->_zkReg.registerActiveService(ServiceFactory::_zkRegPath, ServiceFactory::_svrHost, ServiceFactory::_svrPort, ServiceFactory::_zkScheme)) {
		logger().information("Registered with zookeeper");
	    }
	    this->_zkMonitorReg.setZkHosts(ServiceFactory::_zkServers);
	    if (this->_zkMonitorReg.registerActiveService("/up-monitor/" + ServiceFactory::_zkRegPath, ServiceFactory::_svrHost, ServiceFactory::_cfgPort, "thrift_binary")) {
		logger().information("Registered monitor with zookeeper");
	    }
	}
    } catch (...) {
    }

    waitForTerminationRequest();

    //flush db
    ServiceFactory::getKVStorage()->flush();
    return 0;
}

void ServerApp::defineOptions(Poco::Util::OptionSet& options) {
    Poco::Util::ServerApplication::defineOptions(options);

    options.addOption(Poco::Util::Option("help", "help")
	    .description("show help messages")
	    .binding("app.showhelp")
	    );
}

namespace Up {
namespace Caching {
Def_CaceFactory_FromString64(int64_t, model::ModelData, HasherType)
}
}

//