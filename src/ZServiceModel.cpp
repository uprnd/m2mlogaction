#include "ZServiceModel.h"
#include <iostream>
#include "ServiceFactory.h"
#include "ExceptionServer.h"

namespace Up {
namespace Core {
namespace M2M {
namespace LogAction {

ZServiceModel::ZServiceModel(Poco::SharedPtr<PersistentStorageType> aStorage) : _storage(aStorage) {
}

class get_visitor : public PersistentStorageType::data_visitor {
public:

    get_visitor(TValueResult& return_)
    : _return(return_) {
	// FIXME
	std::cout << __FUNCTION__ << " is called\n";
    }

    virtual bool visit(const PersistentStorageType::TKey& key, PersistentStorageType::TValue& value) {
	if (value == model::ModelData::defVal) throw EXCEPTION_SERVER_TYPE(M2M_LOGACTION, ErrorCode::ERR_ITEM_NOT_FOUND);
	auto &_vl = _return.value;
	value.copyTo(_vl);
	_return.error = ErrorCode::ERR_NONE;
	_return.__isset.value = true;
	return false;
    }
private:
    TValueResult& _return;
};

void ZServiceModel::getK(TValueResult& _return, const TKey key) {
    if (_storage) {
	get_visitor visitor(_return);
	_storage->visit(key, &visitor);
    }
}

class put_visitor : public PersistentStorageType::data_visitor {
public:

    put_visitor(TValueResult& return_, const LogAction::TValue& value)
    : _return(return_), _value(value) {
	// FIXME
	std::cout << __FUNCTION__ << " is called\n";
    }

    virtual bool visit(const PersistentStorageType::TKey& key, PersistentStorageType::TValue& value) {
	value.copyFrom(_value);
	_return.error = ErrorCode::ERR_NONE;
	return true;
    }
private:
    TValueResult& _return;
    const LogAction::TValue& _value;
};

void ZServiceModel::putK(TValueResult& _return, const TKey key, const TValue& value) {
    if (_storage) {
	put_visitor visitor(_return, value);
	_storage->visit(key, &visitor);
    }
}

void ZServiceModel::removeK(TValueResult& _return, const TKey key) {
    if (_storage) {
	_storage->remove(key);
    }
}

void ZServiceModel::listK(TValueResultMapList& _return, const int64_t start, const int32_t limit) {

    int resize = 0;
    try {
	/* get start */
	int begin = 0;
	if (start == 0) begin = 1;
	else begin = start;


	/* check list empty */
	int limitGet = ServiceFactory::limitGet;
	TValueResult tValueResult;
	while (limitGet-- > 0) {
	    getK(tValueResult, begin);
	    if (tValueResult.error == ErrorCode::ERR_NONE) break;
	}
	if (limitGet <= 0) {
	    return;
	}

	/* insert to mapResult */
	int size = (start < limit) ? start : limit;
	_return.resize(size);
	TValueResultMapList::iterator iter = _return.begin();
	iter->tKey = begin;
	iter->tValueResult = tValueResult;
	iter->tValueResult.__isset.value = true;



	/* get list */
	TValueResult result;
	for (int i = 0; i < size - 1; ++i) {
	    resize = i + 1;
	    int index = begin - i - 1;
	    limitGet = ServiceFactory::limitGet;
	    while (limitGet-- > 0) {
		getK(result, index);
		if (result.error == ErrorCode::ERR_NONE) {
		    ++iter;
		    iter->tKey = index;
		    iter->tValueResult = result;
		    iter->tValueResult.__isset.value = true;
		    std::cout << "index = " << index << std::endl;
		    limitGet = 0;
		}
	    }
	}

    } catch (const ExceptionServer& e) {
	std::cerr << "Error list ";
	_return.resize(resize);

    } catch (...) {
	std::cerr << "Error unknown";
	_return.resize(resize);
    }

}



}
}
}
}