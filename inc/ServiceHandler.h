#pragma once

#include "thriftutil/TThriftServer.h"
#include "ZServiceModel.h"
#include "TM2MLogActionService.h"

namespace Up {
    namespace Core {
	namespace M2M {
	    namespace LogAction {

		class ServiceHandler : virtual public TM2MLogActionServiceIf {
		public:
		    typedef up::up101::transport::TThriftServer< TM2MLogActionServiceIf, TM2MLogActionServiceProcessor,
		    ::apache::thrift::protocol::TCompactProtocolFactory> ThriftServer;

		    ServiceHandler(Poco::SharedPtr<ZServiceModel> aModel) {
			_model = aModel;
			_pmodel = _model.get();
		    };
		private:
		    Poco::SharedPtr<ZServiceModel> _model;
		    ZServiceModel* _pmodel;

		public:

		    virtual void putK(TValueResult& _return, const TKey key, const TValue& value) {
			if (_pmodel) {
			    return _pmodel->putK(_return, key, value);
			}
		    }

		    virtual void getK(TValueResult& _return, const TKey key) {
			if (_pmodel) {
			    _pmodel->getK(_return, key);
			}
		    }

		    virtual void removeK(TValueResult& _return, const TKey key) {
			if (_pmodel) {
			    _pmodel->removeK(_return, key);
			}
		    }

		    virtual void listK(TValueResultMapList& _return, const int64_t start, const int32_t limit) {
			if (_pmodel) {
			    _pmodel->listK(_return, start, limit);
			}

		    }
		};

	    }
	}
    }
}
