/* 
 * File:   ServerApp.h
 * Author: trungthanh
 *
 * Created on February 23, 2012, 6:18 PM
 */

#ifndef SERVERAPP_H
#define	SERVERAPP_H


#include <Poco/Util/ServerApplication.h>
#include <Poco/Util/HelpFormatter.h>
#include <Poco/NumberParser.h>
#include <Poco/Net/ServerSocket.h>
#include <vector>
#include <iostream>

#include "upframework/upbase/base/UPZKService.h"

class ServerApp : public Poco::Util::ServerApplication {
public:
    ServerApp();
    ServerApp(const ServerApp& orig);
    virtual ~ServerApp();

    void showHelp() {
	Poco::Util::HelpFormatter helpFormatter(options());
	helpFormatter.setCommand(commandName());

	helpFormatter.setUsage("[OPTIONS]\n");
	helpFormatter.setHeader("Options ");
	helpFormatter.format(std::cout);
    }

    void defineOptions(Poco::Util::OptionSet& options);
    void initialize(Poco::Util::Application& app);
    virtual int main(const std::vector<std::string>& args);

private:
    bool _showHelp;
    UPZKServiceRegister _zkReg;
    UPZKServiceRegister _zkMonitorReg;
};

#endif	/* SERVERAPP_H */

