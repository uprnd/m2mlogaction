#pragma once

#include "Storage/CachePersistent.h"
#include "Caching/AbstractCache.h"
#include "Caching/CacheFactory.h"
#include "thriftutil/TSerializer.h"
#include "Hashing/DefaultHasher.h"
#include "m2mlogaction_types.h"
#include "ModelData.h"

namespace Up {
    namespace Core {
	namespace M2M {
	    namespace LogAction {


		typedef Up::Storage::ObjectStorage< TKey, model::ModelData, TThriftSerializer<TValue> > BackendStorageType;
		typedef Up::Storage::CachePersistent< TKey, model::ModelData, BackendStorageType > PersistentStorageType;
		typedef PersistentStorageType::CacheType CacheType;
		typedef Up::Hashing::DefaultHasher HasherType;
		typedef Up::Caching::BasicCacheFactory< TKey, model::ModelData, HasherType > CacheFactory;

	    }
	}
    }
}

