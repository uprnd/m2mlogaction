/*
 * File:   ModelData.h
 * Author: tiennd
 *
 * Created on November 7, 2014, 5:36 PM
 */

#ifndef MODELDATA_H
#define	MODELDATA_H



#include <string>
#include <cstdlib>
#include <iostream>
#include <vector>
#include "m2mlogaction_types.h"


namespace Up {
    namespace Core {
	namespace M2M {
	    namespace LogAction {

		namespace model {

		    class ModelData : public TValue {
		    public:

			inline void copyFrom(const TValue& v) {
			    (TValue&) * this = v;
			}

			inline void copyTo(TValue& v) const {
			    v = *this;
			}

			static ModelData defVal;

		    };

		}
	    }
	}
    }
}



#endif	/* MODELDATA_H */

