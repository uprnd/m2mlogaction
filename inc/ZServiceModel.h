#pragma once

#include "storagedef.h"
#include "m2mlogaction_types.h"

namespace Up {
    namespace Core {
	namespace M2M {
	    namespace LogAction {

		class ZServiceModel {
		public:
		    ZServiceModel(Poco::SharedPtr<PersistentStorageType> aStorage);
		public:

		    void putK(TValueResult& _return, const TKey key, const TValue& value);
		    void getK(TValueResult& _return, const TKey key);
		    void removeK(TValueResult& _return, const TKey key);
		    void listK(TValueResultMapList& _return, const int64_t start, const int32_t limit);

		private:
		    ZServiceModel(const ZServiceModel& orig);
		    Poco::SharedPtr<PersistentStorageType> _storage;

		};

	    }
	}
    }
}

