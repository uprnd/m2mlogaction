#pragma once

#include "ServiceFactoryT.h"
#include "storagedef.h"
#include "ZServiceModel.h"

namespace Up {
    namespace Core {
	namespace M2M {
	    namespace LogAction {

		class ServiceFactory : public ServiceFactoryT< CacheType, CacheFactory, BackendStorageType, PersistentStorageType, ZServiceModel> {
		public:
		    static void init(Poco::Util::Application& app);
		    static int limitGet;

		private:
		    typedef ServiceFactoryT< CacheType, CacheFactory, BackendStorageType, PersistentStorageType, ZServiceModel> _Base;
		};

	    }
	}
    }
}